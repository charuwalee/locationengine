#ifndef CCPKF_H
#define CCPKF_H

#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "deca_util.h"

#include <QDebug>
#include <armadillo>

using namespace arma;

#define KF_SUCCESS                      0
#define KF_ERR_NON_VECTOR_INPUT_DATA   -1  //error value indicating that some input required as a vector is ill-format.
#define KF_ERR_WRONG_SIZE_INPUT_DATA   -2  //error value indicating that some input matrix is provided with wrong size.
#define KF_ERR_MATRIX_INVERSE_FAIL     -3

class CCPKF
{
public:
    CCPKF();
};


//typedef struct {
//     vec        x_hat;      // "t2,k-1 ; f12,k-1"
//     vec        u;          // "0"
//     vec        z;          // "t2,k"
//     mat        F;          // "1.0  dt ; 0.0  1.0"
//     mat        B;          // "0.0 ; 0.0"
//     mat        H;          // "1.0   0.0"
//     mat        Q;          // "5E-20  15E-40 ; 15E-40  5E-20"
//     mat        R;          // "3E-20"
//     mat        P_hat;      // ""0.0  0.0 ; 0.0  0.0""
//     bool       lastUpdateSuccess;
//} kfdata_struct_t;


typedef struct {
    double      slave_clk_hat;       // in seconds
    double      rel_freq_skew_hat;
    double      slave_clk_reading;   // in seconds
    double      ccp_period;          // in seconds
    double      P00;
    double      P01;
    double      P10;
    double      P11;
    bool        lastUpdateSuccess;
} ccp_kfdata_struct_t;


//int getNewEstimate (kfdata_struct_t *data);
int getNewEstimate (ccp_kfdata_struct_t *data);

#endif // KF_H
