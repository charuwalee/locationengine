#ifndef DECA_UTIL_H
#define DECA_UTIL_H

#define TIME_UNITS                  (1.0/499.2e6/128.0)                 // 15.65 ps
#define CLOCK_WRAP_PERIOD           (0x10000000000LL)                   // period is 40 bits in DW1000
#define CLOCK_HALF_WRAP_PERIOD      (0x8000000000LL)                    // half of CLOCK_PERIOD
#define CLOCK_WRAP_PERIOD_SEC       (CLOCK_WRAP_PERIOD * TIME_UNITS)    // 17.2073569746944 s
#define CLOCK_HALF_WRAP_PERIOD_SEC  (CLOCK_WRAP_PERIOD_SEC/2.0)         //  8.6036784873472 s
#define SPEED_OF_LIGHT              (299792458.0/1.000293)              // m/s  or should it be (299792458.0l/1.000293l) ?
#define RANGE_IN_ONE_CLK            (TIME_UNITS * SPEED_OF_LIGHT)       // 0.004690389694435 m

typedef unsigned long long timestamp_t;


double convertdevicetimetosecu(timestamp_t dt);
timestamp_t clocksubtraction(timestamp_t clk1, timestamp_t clk2);
timestamp_t clockaddition(timestamp_t clk1, timestamp_t clk2);

#endif // DECA_UTIL_H
