#include "deca_util.h"

double convertdevicetimetosecu(timestamp_t dt)
{
    double f = 0;

    f = dt * TIME_UNITS ;  // seconds #define TIME_UNITS          (1.0/499.2e6/128.0) = 15.65e-12

    return f ;
}


//Calculate clk1 - clk2 while taking into account the clock wrap around feature.
//Assuming clk1 comes after clk2 and should be in higher value unless the clock wraps around.
timestamp_t clocksubtraction(timestamp_t clk1, timestamp_t clk2)
{
    if (clk1 < clk2)
    {
        //clk1 += 0x10000000000LL;
        clk1 = clk1 + CLOCK_WRAP_PERIOD;
    }

    return  clk1 - clk2;
}


//Calculate clk1 + clk2 while taking into account the clock wrap around feature.
timestamp_t clockaddition(timestamp_t clk1, timestamp_t clk2)
{
    timestamp_t sum = clk1 + clk2;
    if (sum >= CLOCK_WRAP_PERIOD)
    {
        sum -= CLOCK_WRAP_PERIOD;
    }

    return  sum;
}
