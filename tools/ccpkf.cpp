#include "ccpkf.h"
#include "deca_util.h"


CCPKF::CCPKF()
{

}


int getNewEstimate (ccp_kfdata_struct_t *data)
{
    vec x_hat(2);
    x_hat(0) = data->slave_clk_hat;      // in seconds
    x_hat(1) = data->rel_freq_skew_hat;

    mat F(2, 2, fill::eye);
    F(0,1) = data->ccp_period;           // in seconds

    mat H("1.0  0.0");
    mat Q("5E-20  15E-40; 15E-40  5E-20");
    mat R("3E-20");

    vec z(1);
    z(0) = data->slave_clk_reading;

    mat P_hat(2, 2);
    P_hat(0,0) = data->P00;
    P_hat(0,1) = data->P01;
    P_hat(1,0) = data->P10;
    P_hat(1,1) = data->P11;


    //predict
    vec x_predict = (F * x_hat);
    mat P_predict = (F * P_hat * trans(F)) + Q;

    if (x_predict(0) > CLOCK_WRAP_PERIOD_SEC)
    {
        x_predict(0) -= CLOCK_WRAP_PERIOD_SEC;
    }

    //update
    mat tmp1 = P_predict * trans(H);
    mat tmp2;
    if (inv(tmp2, H * tmp1 + R) == false)
    {
        data->lastUpdateSuccess = false;
        return (KF_ERR_MATRIX_INVERSE_FAIL);
    }

    mat K =  tmp1 * tmp2;
    x_hat = x_predict + K * (z - H * x_predict);
    P_hat = (eye<mat>(2,2) - K * H) * P_predict;

    // must check if slave clock estimate is over the clock count limit
    if (x_hat(0) > CLOCK_WRAP_PERIOD_SEC)
    {
        x_hat(0) -= CLOCK_WRAP_PERIOD_SEC;
    }

    data->slave_clk_hat = x_hat(0);
    data->rel_freq_skew_hat = x_hat(1);
    data->P00 = P_hat(0,0);
    data->P01 = P_hat(0,1);
    data->P10 = P_hat(1,0);
    data->P11 = P_hat(1,1);
    data->lastUpdateSuccess = true;
    return (KF_SUCCESS);
}


//int getNewEstimate (kfdata_struct_t *data)
//{
//    //sanity check
//    if ( !data->x_hat.is_vec() ||
//         !data->u.is_vec() ||
//         !data->z.is_vec() )
//    {
//        return (KF_ERR_NON_VECTOR_INPUT_DATA);
//    }

//    uword var_num = data->x_hat.n_rows;
//    uword msig_num = data->z.n_rows;
//    uword isig_num = data->u.n_rows;


//    //other sanity check
//    if ( //size(data->x_hat) != size(data->x_hat_prev) ||
//         //size(data->P_hat) != size(data->P_hat_prev) ||
//         var_num  != data->F.n_rows  ||
//         var_num  != data->F.n_cols  ||
//         var_num  != data->B.n_rows  ||
//         isig_num != data->B.n_cols  ||
//         msig_num != data->H.n_rows  ||
//         var_num  != data->H.n_cols  ||
//         var_num  != data->Q.n_rows  ||
//         var_num  != data->Q.n_cols  ||
//         msig_num != data->R.n_rows  ||
//         msig_num != data->R.n_cols  )
//    {
//        data->lastUpdateSuccess = false;
//        return (KF_ERR_WRONG_SIZE_INPUT_DATA);
//    }


//    //predict
//    vec x_predict = (data->F * data->x_hat) + (data->B * data->u);
//    mat P_predict = (data->F * data->P_hat * trans(data->F)) + data->Q;


//    //update
//    mat tmp1 = P_predict * trans(data->H);
//    mat tmp2;
//    if (inv(tmp2, data->H * tmp1 + data->R) == false)
//    {
//        data->lastUpdateSuccess = false;
//        return (KF_ERR_MATRIX_INVERSE_FAIL);
//    }

//    mat K =  tmp1 * tmp2;
//    data->x_hat = x_predict + K * (data->z - data->H * x_predict);
//    data->P_hat = (eye<mat>(var_num,var_num) - K * data->H) * P_predict;
//    data->lastUpdateSuccess = true;
//    return (KF_SUCCESS);
//}

