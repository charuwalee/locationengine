#ifndef RTLSDISPLAYAPPLICATION_H
#define RTLSDISPLAYAPPLICATION_H

#include <QApplication>
#include <QList>
#include <QSerialPortInfo>


#define DBG_READ_FROM_INPUT_FILE   1


class RTLSClient;
class SerialConnection;
class MainWindow;

class RTLSDisplayApplication : public QApplication
{
    Q_OBJECT
public:
    explicit RTLSDisplayApplication(int &argc, char ** argv);
    virtual ~RTLSDisplayApplication();

    static RTLSDisplayApplication *instance();
    static RTLSClient *client();
    static SerialConnection *serialConnection(QSerialPortInfo x);
    static SerialConnection *serialConnection(QString portName);
    static SerialConnection *serialConnection(int i);
    static MainWindow *mainWindow();
    static void connectReady(QObject *receiver, const char *member, Qt::ConnectionType type = Qt::AutoConnection);

    static int serialPortNum() { return instance()->_serialConnectionList.size(); }

private:
    RTLSClient *_client;
    QList<SerialConnection *> _serialConnectionList;
    QList<QSerialPortInfo>  _portInfo;
    QStringList _portName;
    MainWindow *_mainWindow;
    bool    _ready;

    void findSerialDevices();

signals:
    void ready();

protected slots:
    //void readDataFromFile();
};

#endif // RTLSDISPLAYAPPLICATION_H
