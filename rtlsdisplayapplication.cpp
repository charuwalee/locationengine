#include "rtlsdisplayapplication.h"
#include "mainWindow.h"
#include "rtlsclient.h"
#include "serialconnection.h"
#include <QMetaMethod>
#include <QFile>

#define SERIAL_PORT_NAME_STR                        "cu.usbmodem"

#define MDEK1001_SERIAL_PORT_MANUFACTURER_STR       "SEGGER"
#define MDEK1001_SERIAL_PORT_DESCRIPTION_STR        "J-Link"

#define LAI_TAG_SERIAL_PORT_MANUFACTURER_STR        "STMicroelectronics"
#define LAI_TAG_SERIAL_PORT_DESCRIPTION_STR         "USB ACM"

#define LAI_ANC_V2_SERIAL_PORT_MANUFACTURER_STR     "STMicroelectronics"
#define LAI_ANC_V2_SERIAL_PORT_DESCRIPTION_STR      "USB ACM"


RTLSDisplayApplication::RTLSDisplayApplication(int &argc, char **argv) :
    QApplication (argc, argv),
    _ready(false)
{
    _client = new RTLSClient(this);
    _client->initRangeInClkCnt();

    findSerialDevices();
    for (int i=0; i<_portInfo.size(); i++) {
        QSerialPortInfo port = _portInfo.at(i);
        SerialConnection * serialConn = new SerialConnection(this, _client, &port);
        _serialConnectionList.append(serialConn);
    }

    _mainWindow = new MainWindow();

#if (DBG_READ_FROM_INPUT_FILE)
    QString infilename = "/Users/charuwalee/LAI/experiment/202010_3D_tdoa_with_wireless_clock_sync/20201030_102125_RTLS_log.csv";
    QFile testInFile(infilename);
    _client->openLogFile("/Users/charuwalee/LAI/experiment/202010_3D_tdoa_with_wireless_clock_sync/20201030_102125");

    if ( !testInFile.open(QIODevice::ReadOnly | QIODevice::Text) )
        return;

    while (!testInFile.atEnd()) {
        QByteArray line = testInFile.readLine();

        if (line.length() > 2)
        {
            _client->newDataFromFile(line);
            //_client->newData(line);
        }
    }

    qDebug() << "End of input file " << infilename;
#else
    _client->openLogFile();
    emit ready();
#endif
}


RTLSDisplayApplication::~RTLSDisplayApplication()
{
    qDebug() << "Deleting RTLSDisplayApplication.";
    _client->closeLogFile();
    delete _client;
    while (!_serialConnectionList.isEmpty())
    {
       delete _serialConnectionList.takeFirst();
    }

    delete _mainWindow;
}


RTLSDisplayApplication *RTLSDisplayApplication::instance()
{
    return qobject_cast<RTLSDisplayApplication *>(QCoreApplication::instance());
}


RTLSClient *RTLSDisplayApplication::client()
{
    return instance()->_client;
}


SerialConnection *RTLSDisplayApplication::serialConnection(QSerialPortInfo x)
{
    return RTLSDisplayApplication::serialConnection(x.portName());
}


SerialConnection *RTLSDisplayApplication::serialConnection(QString portName)
{
    RTLSDisplayApplication * inst = instance();
    int portNum = inst->serialPortNum();
    QList<SerialConnection *> sclist = inst->_serialConnectionList;
    int index = 0;

    for (; index<portNum; index++) {
        if (sclist.at(index)->portName() == portName)
            break;
    }

    if (index >= portNum) {
        qDebug() << "RTLSDisplayApplication::serialConnection(): No serial port with specified QSerialPortInfo value found.";
        return nullptr;
    }

    return sclist.at(index);
}


SerialConnection *RTLSDisplayApplication::serialConnection(int i)
{
    return instance()->_serialConnectionList.at(i);
}


MainWindow *RTLSDisplayApplication::mainWindow()
{
    return instance()->_mainWindow;
}


//void RTLSDisplayApplication::readDataFromFile()
//{
//    QFile testInFile("./test/testdata.txt");
//    if ( !testInFile.open(QIODevice::ReadOnly | QIODevice::Text) )
//        return;

//    while (!testInFile.atEnd()) {
//        QByteArray line = testInFile.readLine();

//        _client->newDataFromFile(line);
//    }
//}



void RTLSDisplayApplication::connectReady(QObject *receiver, const char *member, Qt::ConnectionType type)
{
    QMetaMethod method = receiver->metaObject()->method(receiver->metaObject()->indexOfMethod(QMetaObject::normalizedSignature(member)));
        // Either call the method or connect it to the ready signal

        if (instance()->_ready && method.isValid())
            method.invoke(receiver, type);
        else
            QObject::connect(instance(), QMetaMethod::fromSignal(&RTLSDisplayApplication::ready), receiver, method, type);
}


void RTLSDisplayApplication::findSerialDevices()
{
    _portInfo.clear();
    _portName.clear();
    QList<QSerialPortInfo> portList = QSerialPortInfo::availablePorts();

    //foreach (const QSerialPortInfo port, QSerialPortInfo::availablePorts())
    for(int i=0; i<portList.size(); i++)
    {
        QSerialPortInfo port = portList.at(i);
//        qDebug() << "\n\nNo. " << i
//                 << "\nPort name: " << port.portName()
//                 << "\nVendor ID: " << port.vendorIdentifier()
//                 << "\nProduct ID: " << port.productIdentifier()
//                 << "\nHas product ID: " << port.hasProductIdentifier()
//                 << "\nHas vendor ID: " << port.hasVendorIdentifier()
//                 << "\nIs busy: " << port.isBusy()
//                 << "\nManufacturer: " << port.manufacturer()
//                 << "\nDescription " << port.description();

        QString mf = port.manufacturer();
        QString dc = port.description();
        QString portName = port.portName();
        if( portName.contains(SERIAL_PORT_NAME_STR) &&
            ( (mf == MDEK1001_SERIAL_PORT_MANUFACTURER_STR   && dc == MDEK1001_SERIAL_PORT_DESCRIPTION_STR) ||
              (mf == LAI_TAG_SERIAL_PORT_MANUFACTURER_STR    && dc == MDEK1001_SERIAL_PORT_DESCRIPTION_STR) ||
              (mf == LAI_ANC_V2_SERIAL_PORT_MANUFACTURER_STR && dc == LAI_ANC_V2_SERIAL_PORT_DESCRIPTION_STR)
          ) )
        {
            qDebug() << "Port " << portName << " is added to the list.";
            _portInfo.append(port);
            _portName += port.portName();
        }
    }
}
