#ifndef RTLSCLIENT_H
#define RTLSCLIENT_H

#include <QObject>
#include <stdint.h>
#include <QString>
#include <QHash>
#include <ccpkf.h>
#include <deca_util.h>

//#define COMPENSATE_ANTENNA_DELAY_FIX
#define DEFAULT_RX_ANTENNA_DELAY        16418   //0x4022
#define DEFAULT_TX_ANTENNA_DELAY        16418   //0x4022


class QFile;
class QSerialPort;

typedef struct
{
    int     seqNum;
    QVector<int>  recvAncList;
    QVector<int>  cnvtAncList;
} tag_blink_report_t;


//typedef struct
//{
//    double      x, y, z; //in meters
//    uint64_t    id64bit;
//    QString     label;
//} anc_struct_t;


//typedef struct
//{
//    double      x, y, z; // in meters
//    uint64_t    tagid64bit;
//} pos_report_t;


typedef struct
{
    int         seq;
    timestamp_t timestamp;
} tag_blink_info_t;


typedef struct
{
    int          seq;
    timestamp_t  master_clk;
    timestamp_t  slave_clk;
} ccp_db_struct_t;


class RTLSClient : public QObject
{
    Q_OBJECT
public:
   explicit RTLSClient(QObject *parent = nullptr);

    int calculateTagLocation();
    void newDataFromFile(QByteArray data);
    void openLogFile(void);
    void openLogFile(QString prefix);
    void closeLogFile(void);
    void initRangeInClkCnt();

private:
    QFile *_file;
    QFile *_fileDbg;

    QString _logFilePath;
    int     _ccpseq;  //Latest seq value of CCP broadcast from the Master
    QHash<QString, timestamp_t> _rangeInClk;                // "mid:aid" -> timestamp
    QHash<int, int> _currTagBlinkSeqNum;                    // tid -> seqNum

    QHash<QString, timestamp_t> _mccpDb;                    // "mid:seq" -> timestamp
    QHash<QString, ccp_db_struct_t> _ccpDb;                 // "mid:aid" -> {master_timestamp, slave_timestamp}
    QHash<QString, ccp_kfdata_struct_t> _ccpkfDb;           // "mid:aid" -> ccpkfdata
    //QHash<QString, timestamp_t> _recvBlinkTimestamp;      // "aid:tid:seq" -> timestamp
    QHash<QString, tag_blink_info_t> _recvBlinkTimestamp;   // "aid:tid" -> {seq, timestamp}  keep only the last blink the anchor receives from the tag
    QHash<int, tag_blink_report_t> _tagBlinkReport;         // tid -> tag_blink_report_t
    QHash<QString, timestamp_t> _cnvtRecvBlinkTimestamp;    // "aid:tid:seq" -> timestamp
    QHash<QString, double> _cnvtRecvBlinkTimeInSeconds;     // "aid:tid:seq" -> double
    void writeToLogFile(QByteArray data);
    void writeToLogFile(QString str);
    void writeToDbgFile(QByteArray data);
    void writeToDbgFile(QString str);
    void processCCPMaster(QTime now, int mid, int seq, timestamp_t masterTxTimestamp);
    void processCCPSlave(QTime now, int mid, int aid, int seq, timestamp_t masterPrevTimestamp, timestamp_t masterCurrTimestamp, timestamp_t receivedTimestamp);
    void processTagBlink(QTime now, int tid, int aid, int seq, timestamp_t receivedTimestamp);
    void insertRecvTagBlinkTimestamp (int aid, int tid, int seq, timestamp_t localTimestamp);
    void insertCCPDb(int mid, int aid, int seq, timestamp_t masterTimestamp, timestamp_t slaveTimestamp);
    void insertCCPKFDbAndUpdate(int mid, int aid, int seq, timestamp_t masterPrevTimestamp, timestamp_t masterCurrTimestamp, timestamp_t receivedTimestamp);
    ccp_kfdata_struct_t initCCPKFData(int mid, int aid, int seq, timestamp_t masterPrevTimestamp, timestamp_t masterCurrTimestamp, timestamp_t slaveTimestamp);
    bool isTagBlinkReceived(QString str, int seq); //check if the recently received blink of the tag by the anchor has the seq number.
    //void newDataCommon(QByteArray data);

protected slots:
    void newData(QByteArray);
    //void onReady();
    //void onConnected(QString portName);

};


#endif // RTLSCLIENT_H
