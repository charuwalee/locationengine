#ifndef SERIALCONNECTION_H
#define SERIALCONNECTION_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include "rtlsclient.h"


class SerialConnection : public QObject
{
    Q_OBJECT
public:
    explicit SerialConnection(QObject *parent = nullptr, RTLSClient *client = nullptr, QSerialPortInfo *x = nullptr);
    ~SerialConnection();

//    enum ConnectionState
//    {
//        Disconnected = 0,
//        Connecting,
//        Connected,
//        ConnectionFailed
//    };

//  void findSerialDevices();
    QSerialPort* serialPort() { return _serial; }
    int openConnection(QSerialPortInfo x);
    void closeConnection();
//  QStringList portList();
    QString portName();


signals:
    void dataReceived(QByteArray);

protected slots:
    void handleError(QSerialPort::SerialPortError err);
    void onConnected(void);
    void readData(void);

private:
//  int openSerialPort(QSerialPortInfo x);

    QSerialPort * _serial;
//  QList<QSerialPortInfo> _portInfo;
//  QStringList _portName;
    int _portIndex;
};

#endif // SERIALCONNNECTION_H
