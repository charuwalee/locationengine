#include "serialconnection.h"
#include "rtlsdisplayapplication.h"
#include <QDebug>
#include <QSerialPortInfo>


SerialConnection::SerialConnection(QObject *parent, RTLSClient *client, QSerialPortInfo *x) :
    QObject(parent)
{
    _serial = new QSerialPort(this);
    connect(this, SIGNAL(dataReceived(QByteArray)), client, SLOT(newData(QByteArray)));
    connect(RTLSDisplayApplication::instance(), SIGNAL(ready()), this, SLOT(onConnected()));
    openConnection(*x);
}


SerialConnection::~SerialConnection()
{
    if (_serial->isOpen())
    {
        qDebug() << "Closing serial port " << _serial->portName() << ".";
        _serial->close();
    }

    qDebug() << "Deleting SerialConnection.";

    delete _serial;
}


//Once connected with RTLSClient, start reading from serial port
void SerialConnection::onConnected()
{
    connect(_serial, SIGNAL(readyRead()), this, SLOT(readData()));
}


void SerialConnection::readData(void)
{
    // Once this slot is triggered by _serial's readyRead signal,
    // the RTLSClient may not be ready to process the data
    // the _processingData flag and serialOpened signal are used to sync with the RTLSClient.
    // The serialOpened signal is connected to RTLSClient::onConnected slot in RTLSClient::onReady() function.

    QByteArray data = _serial->readAll();
    int length = data.length();

    if (length > 0)
    {
        //emit dataReceived(_serial->portName(), data);
        emit dataReceived(data);
    }
}


int SerialConnection::openConnection(QSerialPortInfo x)
{
    int error = 0;
    _serial->setPort(x);

    if (!_serial->isOpen())
    {
        if (_serial->open(QIODevice::ReadOnly))
        {
            _serial->setBaudRate(QSerialPort::Baud115200);
            _serial->setDataBits(QSerialPort::Data8);
            _serial->setParity(QSerialPort::NoParity);
            _serial->setStopBits(QSerialPort::OneStop);
            _serial->setFlowControl(QSerialPort::NoFlowControl);
            qDebug() << "Successfully open serial port name: " << _serial->portName() << ".";
        }
        else {
            qDebug() << "SerialConnection::openSerialPort(): Could not open serial port name: " << (x).portName()
                     << " with error " << _serial->error();
            _serial->close();
            error = 1;
        }
    }
    else {
        qDebug() << "Serial port " << _serial->portName() << " is already open!";
    }

    return error;
}


void SerialConnection::closeConnection()
{
    _serial->close();
    qDebug() << "SerialConnection::closeConnection(): closing serial port " << _serial->portName() << ".";
//    _processingData = true;
}


QString SerialConnection::portName()
{
    return _serial->portName();
}


void SerialConnection::handleError(QSerialPort::SerialPortError err)
{
    qDebug() << "SerialConnection::handleError(): serial port " << _serial->portName() << " with error " << err << ".";
    _serial->close();
//    _processingData = true;
}


//void SerialConnection::findSerialDevices()
//{
//    _portInfo.clear();
//    _portName.clear();

//    foreach (const QSerialPortInfo &port, QSerialPortInfo::availablePorts())
//    {
//        //Their is some sorting to do for just list the port I want, with vendor Id & product Id
//        qDebug() << "\n\nPort name: " << port.portName()
//                 << "\nVendor ID: " << port.vendorIdentifier()
//                 << "\nProduct ID: " << port.productIdentifier()
//                 << "\nHas product ID: " << port.hasProductIdentifier()
//                 << "\nHas vendor ID: " << port.hasVendorIdentifier()
//                 << "\nIs busy: " << port.isBusy()
//                 << "\nManufacturer: " << port.manufacturer()
//                 << "\nDescription " << port.description();

//        QString mf = port.manufacturer();
//        QString dc = port.description();
//        if( (mf == MDEK1001_SERIAL_PORT_MANUFACTURER_STR   && dc == MDEK1001_SERIAL_PORT_DESCRIPTION_STR) ||
//            (mf == LAI_TAG_SERIAL_PORT_MANUFACTURER_STR    && dc == MDEK1001_SERIAL_PORT_DESCRIPTION_STR) ||
//            (mf == LAI_ANC_V2_SERIAL_PORT_MANUFACTURER_STR && dc == LAI_ANC_V2_SERIAL_PORT_DESCRIPTION_STR) )
//        {
//            _portInfo += port;
//            _portName += port.portName();

//            connect(_serial, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(handleError(QSerialPort::SerialPortError)));
//            connect(_serial, SIGNAL(readyRead()), this, SLOT(readData()));
//        }
//    }
//}


//void SerialConnection::openConnection()
//{
//    foreach (const QSerialPortInfo &port, _portInfo)
//    {
//          openSerialPort(port);
//    }
//}

