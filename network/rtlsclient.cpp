#include "rtlsclient.h"
#include "rtlsdisplayapplication.h"
#include "serialconnection.h"
#include "deca_util.h"

#include <QTextStream>
#include <QDateTime>
#include <QString>
#include <QFile>
#include <QStringBuilder>
#include <QDir>
#include <QtMath>



#define GEN_LOG_FILE                    0 // The _RTLS_log.txt file used to log data from USB port
#define GEN_DBG_FILE                    1 // The _RTLS_log_dbg.txt file used to analyse the log (real-time) or csv (offline) file

#define MIN_REQUIRED_ANC_NUM            (5)
#define SYS_MASTER_ID                   (0)



#define FUNCCODE_CHAR_LEN               (4)  //eg. "0x83"
#define DEVICE_ID_CHAR_LEN              (6)  //eg. "0x0001"
#define SEQUENCE_CHAR_LEN               (4)  //eg. "0xAA"
#define TIMESTAMP_CHAR_LEN              (12) //eg. "0x1122334455"

#define CCP_MASTER_REPORT_NO_FCODE_CHAR_LEN      (24) //6,4,12
#define CCP_SLAVE_REPORT_NO_FCODE_CHAR_LEN       (57) //6,6,4,12,12,12
#define TAG_BLINK_REPORT_NO_FCODE_CHAR_LEN       (31) //6,6,4,12

#define CCP_MASTER_REPORT_ARG_NUM       (4)
#define CCP_SLAVE_REPORT_ARG_NUM        (7)
#define TAG_BLINK_REPORT_ARG_NUM        (5)

#define TAG_BLINK_FUNCCODE              "0x83"
#define CCP_MASTER_FUNCCODE             "0x84"
#define CCP_SLAVE_FUNCCODE              "0x85"


RTLSClient::RTLSClient(QObject *parent) :
    QObject(parent),
    _file(nullptr),
    _fileDbg(nullptr)
{
//    QHash<QString, int> _tagList;
    QHash<QString, timestamp_t> _mccpDb;
    QHash<QString, ccp_db_struct_t> _ccpDb;
    QHash<QString, int> _currTagBlinkSeqNum;
    QHash<QString, timestamp_t> _recvBlinkTimestamp;
    QHash<int, tag_blink_report_t> _tagBlinkReport;
    QHash<QString, timestamp_t> _cnvtRecvBlinkTimestamp;
    QHash<QString, double> _cnvtRecvBlinkTimeInSeconds;     //****** for computational perfomance testing ****
    QHash<QString, timestamp_t> _rangeInClk;
}


void RTLSClient::initRangeInClkCnt()
{
    timestamp_t clkcnt;
    qreal       a0x=0.00, a0y=0.00, a0z=1.15,
                a1x=0.00, a1y=1.70, a1z=1.15,
                a2x=0.90, a2y=3.00, a2z=1.15,
                a3x=2.10, a3y=3.00, a3z=1.15,
                a4x=2.92, a4y=1.30, a4z=1.02,
                a5x=2.00, a5y=0.07, a5z=1.02;


    //anc0-anc1
    clkcnt = qSqrt(qPow(a0x-a1x,2)+qPow(a0y-a1y,2)+qPow(a0z-a1z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("0:1", clkcnt);
    qDebug() << QString("range_in_clock a0-a1 %1.").arg(clkcnt);


    //anc0-anc2
    clkcnt = qSqrt(qPow(a0x-a2x,2)+qPow(a0y-a2y,2)+qPow(a0z-a2z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("0:2", clkcnt);
    qDebug() << QString("range_in_clock a0-a2 %1.").arg(clkcnt);

    //anc0-anc3
    clkcnt = qSqrt(qPow(a0x-a3x,2)+qPow(a0y-a3y,2)+qPow(a0z-a3z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("0:3", clkcnt);
    qDebug() << QString("range_in_clock a0-a3 %1.").arg(clkcnt);

    //anc0-anc4
    clkcnt = qSqrt(qPow(a0x-a4x,2)+qPow(a0y-a4y,2)+qPow(a0z-a4z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("0:4", clkcnt);
    qDebug() << QString("range_in_clock a0-a4 %1.").arg(clkcnt);

    //anc0-anc5
    clkcnt = qSqrt(qPow(a0x-a5x,2)+qPow(a0y-a5y,2)+qPow(a0z-a5z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("0:5", clkcnt);
    qDebug() << QString("range_in_clock a0-a5 %1.").arg(clkcnt);

    //anc1-anc0
    clkcnt = qSqrt(qPow(a1x-a0x,2)+qPow(a1y-a0y,2)+qPow(a1z-a0z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("1:0", clkcnt);
    qDebug() << QString("range_in_clock a1-a0 %1.").arg(clkcnt);

    //anc1-anc2
    clkcnt = qSqrt(qPow(a1x-a2x,2)+qPow(a1y-a2y,2)+qPow(a1z-a2z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("1:2", clkcnt);
    qDebug() << QString("range_in_clock a1-a2 %1.").arg(clkcnt);

    //anc1-anc3
    clkcnt = qSqrt(qPow(a1x-a3x,2)+qPow(a1y-a3y,2)+qPow(a1z-a3z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("1:3", clkcnt);
    qDebug() << QString("range_in_clock a1-a3 %1.").arg(clkcnt);

    //anc1-anc4
    clkcnt = qSqrt(qPow(a1x-a4x,2)+qPow(a1y-a4y,2)+qPow(a1z-a4z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("1:4", clkcnt);
    qDebug() << QString("range_in_clock a1-a4 %1.").arg(clkcnt);

    //anc1-anc5
    clkcnt = qSqrt(qPow(a1x-a5x,2)+qPow(a1y-a5y,2)+qPow(a1z-a5z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("1:5", clkcnt);
    qDebug() << QString("range_in_clock a1-a5 %1.").arg(clkcnt);

    //anc2-anc0
    clkcnt = qSqrt(qPow(a2x-a0x,2)+qPow(a2y-a0y,2)+qPow(a2z-a0z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("2:0", clkcnt);
    qDebug() << QString("range_in_clock a2-a0 %1.").arg(clkcnt);

    //anc2-anc1
    clkcnt = qSqrt(qPow(a2x-a1x,2)+qPow(a2y-a1y,2)+qPow(a2z-a1z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("2:1", clkcnt);
    qDebug() << QString("range_in_clock a2-a1 %1.").arg(clkcnt);

    //anc2-anc3
    clkcnt = qSqrt(qPow(a2x-a3x,2)+qPow(a2y-a3y,2)+qPow(a2z-a3z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("2:3", clkcnt);
    qDebug() << QString("range_in_clock a2-a3 %1.").arg(clkcnt);

    //anc2-anc4
    clkcnt = qSqrt(qPow(a2x-a4x,2)+qPow(a2y-a4y,2)+qPow(a2z-a4z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("2:4", clkcnt);
    qDebug() << QString("range_in_clock a2-a4 %1.").arg(clkcnt);

    //anc2-anc5
    clkcnt = qSqrt(qPow(a2x-a5x,2)+qPow(a2y-a5y,2)+qPow(a2z-a5z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("2:5", clkcnt);
    qDebug() << QString("range_in_clock a2-a5 %1.").arg(clkcnt);

    //anc3-anc0
    clkcnt = qSqrt(qPow(a3x-a0x,2)+qPow(a3y-a0y,2)+qPow(a3z-a0z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("3:0", clkcnt);
    qDebug() << QString("range_in_clock a3-a0 %1.").arg(clkcnt);

    //anc3-anc1
    clkcnt = qSqrt(qPow(a3x-a1x,2)+qPow(a3y-a1y,2)+qPow(a3z-a1z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("3:1", clkcnt);
    qDebug() << QString("range_in_clock a3-a1 %1.").arg(clkcnt);

    //anc3-anc2
    clkcnt = qSqrt(qPow(a3x-a2x,2)+qPow(a3y-a2y,2)+qPow(a3z-a2z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("3:2", clkcnt);
    qDebug() << QString("range_in_clock a3-a2 %1.").arg(clkcnt);

    //anc3-anc4
    clkcnt = qSqrt(qPow(a3x-a4x,2)+qPow(a3y-a4y,2)+qPow(a3z-a4z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("3:4", clkcnt);
    qDebug() << QString("range_in_clock a3-a4 %1.").arg(clkcnt);

    //anc3-anc5
    clkcnt = qSqrt(qPow(a3x-a5x,2)+qPow(a3y-a5y,2)+qPow(a3z-a5z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("3:5", clkcnt);
    qDebug() << QString("range_in_clock a3-a5 %1.").arg(clkcnt);

    //anc4-anc0
    clkcnt = qSqrt(qPow(a4x-a0x,2)+qPow(a4y-a0y,2)+qPow(a4z-a0z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("4:0", clkcnt);
    qDebug() << QString("range_in_clock a4-a0 %1.").arg(clkcnt);

    //anc4-anc1
    clkcnt = qSqrt(qPow(a4x-a1x,2)+qPow(a4y-a1y,2)+qPow(a4z-a1z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("4:1", clkcnt);
    qDebug() << QString("range_in_clock a4-a1 %1.").arg(clkcnt);

    //anc4-anc2
    clkcnt = qSqrt(qPow(a4x-a2x,2)+qPow(a4y-a2y,2)+qPow(a4z-a2z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("4:2", clkcnt);
    qDebug() << QString("range_in_clock a4-a2 %1.").arg(clkcnt);

    //anc4-anc3
    clkcnt = qSqrt(qPow(a4x-a3x,2)+qPow(a4y-a3y,2)+qPow(a4z-a3z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("4:3", clkcnt);
    qDebug() << QString("range_in_clock a4-a3 %1.").arg(clkcnt);

    //anc4-anc5
    clkcnt = qSqrt(qPow(a4x-a5x,2)+qPow(a4y-a5y,2)+qPow(a4z-a5z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("4:5", clkcnt);
    qDebug() << QString("range_in_clock a4-a5 %1.").arg(clkcnt);

    //anc5-anc0
    clkcnt = qSqrt(qPow(a5x-a0x,2)+qPow(a5y-a0y,2)+qPow(a5z-a0z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("5:0", clkcnt);
    qDebug() << QString("range_in_clock a5-a0 %1.").arg(clkcnt);

    //anc5-anc1
    clkcnt = qSqrt(qPow(a5x-a1x,2)+qPow(a5y-a1y,2)+qPow(a5z-a1z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("5:1", clkcnt);
    qDebug() << QString("range_in_clock a5-a1 %1.").arg(clkcnt);

    //anc5-anc2
    clkcnt = qSqrt(qPow(a5x-a2x,2)+qPow(a5y-a2y,2)+qPow(a5z-a2z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("5:2", clkcnt);
    qDebug() << QString("range_in_clock a5-a2 %1.").arg(clkcnt);

    //anc5-anc3
    clkcnt = qSqrt(qPow(a5x-a3x,2)+qPow(a5y-a3y,2)+qPow(a5z-a3z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("5:3", clkcnt);
    qDebug() << QString("range_in_clock a5-a3 %1.").arg(clkcnt);

    //anc5-anc4
    clkcnt = qSqrt(qPow(a5x-a4x,2)+qPow(a5y-a4y,2)+qPow(a5z-a4z,2))/RANGE_IN_ONE_CLK;
    _rangeInClk.insert("5:4", clkcnt);
    qDebug() << QString("range_in_clock a5-a4 %1.").arg(clkcnt);
}


void RTLSClient::newDataFromFile(QByteArray data)
{
    newData(data);
}


void RTLSClient::newData(QByteArray data)
{
    int         length = data.length();
    int         offset = 0;

    //QDateTime now = QDateTime::currentDateTime();
    QTime now = QDateTime::currentDateTime().time();

#if (GEN_LOG_FILE)
    writeToLogFile(data);
//    QString s(data);
//    writeToLogFile(QString("logtime %1, %2")
//                    .arg(now.toString("hhmmss.zzz"))
//                    .arg(s));
#endif

    // CCP_MASTER_REPORT_CHAR_LEN is the shortest message length
    // Note that, new data may contain several messages
    while (length > CCP_MASTER_REPORT_NO_FCODE_CHAR_LEN)
    {
        QByteArray funcCode;
        //first, look for valid funcCode
        while (length > CCP_MASTER_REPORT_NO_FCODE_CHAR_LEN)
        {
            funcCode = data.mid(offset, FUNCCODE_CHAR_LEN);

            if (funcCode.startsWith(CCP_MASTER_FUNCCODE) ||
                funcCode.startsWith(CCP_SLAVE_FUNCCODE)  ||
                funcCode.startsWith(TAG_BLINK_FUNCCODE) )
            {
                break;
            }
            offset ++;
            length --;
        }



        int aid, tid, mid;
        int seq;
        timestamp_t receivedTimestamp;
        offset += FUNCCODE_CHAR_LEN+1;
        length -= FUNCCODE_CHAR_LEN+1;
        QByteArray report = data.mid(offset,length);

        //TAG BLINK REPORT
        if (funcCode.startsWith(TAG_BLINK_FUNCCODE))
        {
            //check if all parameters are received
            int n = sscanf(report.constData(),"0x%4x,0x%4x,0x%2x,0x%10llx", &tid, &aid, &seq, &receivedTimestamp);
            offset += (TAG_BLINK_REPORT_NO_FCODE_CHAR_LEN+1);
            length -= (TAG_BLINK_REPORT_NO_FCODE_CHAR_LEN+1);

//            if (n != TAG_BLINK_REPORT_ARG_NUM-1) //excluding funcCode
//            {
//                writeToDbgFile(QString("RTLSClient::newData(): Tag blink report received with some parameters missing :" % report));
//                qDebug() << "RTLSClient::newData(): Tag blink report received with some parameters missing: " << report;
//                return;
//            }

            processTagBlink(now, tid, aid, seq, receivedTimestamp);
        }


        //CCP MASTER REPORT
        else if (funcCode.startsWith(CCP_MASTER_FUNCCODE))
        {
            timestamp_t masterTxTimestamp;
            //check if all parameters are received
            int n = sscanf(report.constData(),"0x%4x,0x%2x,0x%10llx", &mid, &seq, &masterTxTimestamp);
            offset += (CCP_MASTER_REPORT_NO_FCODE_CHAR_LEN+1);
            length -= (CCP_MASTER_REPORT_NO_FCODE_CHAR_LEN+1);

//            if (n != CCP_MASTER_REPORT_ARG_NUM-1) //excluding funcCode
//            {
//                writeToDbgFile(QString("RTLSClient::newData(): CCP master report received with some parameters missing: " % report));
//                qDebug() << "RTLSClient::newData(): CCP master report received with some parameters missing: " << report;
//                continue;
//            }

            processCCPMaster(now, mid, seq, masterTxTimestamp);
        }


        //CCP SLAVE REPORT
        else if (funcCode.startsWith(CCP_SLAVE_FUNCCODE))
        {
            timestamp_t masterPrevTimestamp, masterCurrTimestamp;
            //check if all parameters are received
            int n = sscanf(report.constData(),"0x%4x,0x%4x,0x%2x,0x%10llx,0x%10llx,0x%10llx", &mid, &aid, &seq, &masterPrevTimestamp, &masterCurrTimestamp, &receivedTimestamp);
            offset += (CCP_SLAVE_REPORT_NO_FCODE_CHAR_LEN+1);
            length -= (CCP_SLAVE_REPORT_NO_FCODE_CHAR_LEN+1);

//            if (n != CCP_SLAVE_REPORT_ARG_NUM-1) //excluding funcCode
//            {
//                writeToDbgFile(QString("RTLSClient::newData(): CCP slave report received with some parameters missing: " % report));
//                qDebug() << "RTLSClient::newData(): CCP slave report received with some parameters missing: " << report;
//                continue;
//            }

            processCCPSlave(now, mid, aid, seq, masterPrevTimestamp, masterCurrTimestamp, receivedTimestamp);
        }

    }
}


void RTLSClient::processCCPSlave(QTime now, int mid, int aid, int seq, timestamp_t masterPrevTimestamp, timestamp_t masterCurrTimeStamp, timestamp_t receivedTimestamp)
{
#ifdef COMPENSATE_ANTENNA_DELAY_FIX
    masterPrevTimestamp = clockaddition(masterPrevTimestamp, DEFAULT_TX_ANTENNA_DELAY);
    masterCurrTimeStamp = clockaddition(masterCurrTimeStamp, DEFAULT_TX_ANTENNA_DELAY);
    receivedTimestamp = clocksubtraction(receivedTimestamp, DEFAULT_RX_ANTENNA_DELAY);
#endif

    // Remove this sanity check.
    // It is either the CLE misses a master CCP and still keeps the old version of master clk with the same seqNum,
    // or the CLE receives the CCP reports from slaves before that of the master.
    // Therefor, add new info from slave anyway.
    //sanity check
//    QString str = QString("%1:%2").arg(mid).arg(seq);
//    if (_mccpDb.contains(str))
//    {
//        timestamp_t mccpVal = _mccpDb.value(str);
//        if (masterCurrTimeStamp != mccpVal)
//        {
//            writeToDbgFile(QString("RTLSClient::processCCPSlave(): Mismatch master's timestamp: _mccpDb(ID=0x%1;seqNum=0x%2) is 0x%3 and in slave CCP is 0x%4").arg(mid, 2, 16).arg(seq, 2, 16).arg(mccpVal, 10, 16).arg(masterCurrTimeStamp, 10, 16));
//            qDebug() << "RTLSClient::processCCPSlave(): Mismatch master's timestamp with ccpDb.";
//        }
//    }


    //remove the TOF from the slave's received timestamp
//  receivedTimestamp -= _rangeInClk.value(QString("%1:%2").arg(mid).arg(aid));
    receivedTimestamp = clocksubtraction(receivedTimestamp, _rangeInClk.value(QString("%1:%2").arg(mid).arg(aid)));

    //insert data into ccp DB
    insertCCPDb(mid, aid, seq, masterCurrTimeStamp, receivedTimestamp);
    writeToDbgFile(QString("insertCCPDb,mid,0x%1,aid,0x%2,seq,0x%3,m_ts_hex,%4,m_ts_sec,%5,s_ts_hex,%6,s_sec,%7,logtime,%8")
                   .arg(mid,2,16,QChar('0'))
                   .arg(aid,2,16,QChar('0'))
                   .arg(seq,2,16,QChar('0'))
                   .arg(masterCurrTimeStamp,10,16,QChar('0'))
                   .arg(convertdevicetimetosecu(masterCurrTimeStamp),0,'E',12)
                   .arg(receivedTimestamp,10,16,QChar('0'))
                   .arg(convertdevicetimetosecu(receivedTimestamp),0,'E',12)
                   .arg(now.toString("hhmmss.zzz")));

    insertCCPKFDbAndUpdate(mid, aid, seq, masterPrevTimestamp, masterCurrTimeStamp, receivedTimestamp);
}


void RTLSClient::processCCPMaster(QTime now, int mid, int seq, timestamp_t masterTxTimestamp)
{
#ifdef COMPENSATE_ANTENNA_DELAY_FIX
    masterTxTimestamp = clockaddition(masterTxTimestamp, DEFAULT_TX_ANTENNA_DELAY);
#endif

    //no special process at the moment, just keep log
    _mccpDb.insert(QString("%1:%2").arg(mid).arg(seq), masterTxTimestamp);
    _ccpseq = seq;
    writeToDbgFile(QString("injectMCCP,mid,0x%1,seq,0x%2,m_ts_hex,%3,m_ts_sec,%4,logtime,%5")
                   .arg(mid,2,16,QChar('0'))
                   .arg(seq,2,16,QChar('0'))
                   .arg(masterTxTimestamp,10,16,QChar('0'))
                   .arg(convertdevicetimetosecu(masterTxTimestamp),0,'E',12)
                   .arg(now.toString("hhmmss.zzz")));
}


void RTLSClient::processTagBlink(QTime now, int tid, int aid, int seq, timestamp_t receivedTimestamp)
{
    //QString blinkTimestampKeyString = QString("%1:%2").arg(aid).arg(tid);

    insertRecvTagBlinkTimestamp(aid, tid,seq, receivedTimestamp);
    writeToDbgFile(QString("insertRecvBlinkTS,aid,0x%1,tid,0x%2,seq,0x%3,recv_ts_hex,0x%4,recv_sec,%5,logtime,%6")
                   .arg(aid,2,16,QChar('0'))
                   .arg(tid,2,16,QChar('0'))
                   .arg(seq,2,16,QChar('0'))
                   .arg(receivedTimestamp,10,16,QChar('0'))
                   .arg(convertdevicetimetosecu(receivedTimestamp),0,'E',12)
                   .arg(now.toString("hhmmss.zzz")));

    if (!_currTagBlinkSeqNum.contains(tid) || (seq != _currTagBlinkSeqNum.value(tid)))
    {
        //this blink seqNum from this tagID is newly reported to the CLE
        _currTagBlinkSeqNum.insert(tid, seq);

        //init tag blink report
        tag_blink_report_t blinkReport;
        blinkReport.seqNum = seq;
        blinkReport.recvAncList.clear();
        blinkReport.recvAncList.append(aid);
        blinkReport.cnvtAncList.clear();
        _tagBlinkReport.insert(tid, blinkReport);
    }
    else {
        //this blink seqNum from this tagID has been reported by some other anchors
        tag_blink_report_t blinkReport = _tagBlinkReport.value(tid);

        //keep track of the number of blink report of this tagID-seqNum pair
        if (!blinkReport.recvAncList.contains(aid))
        {
            blinkReport.recvAncList.append(aid);

            if (blinkReport.recvAncList.size() >= MIN_REQUIRED_ANC_NUM)
            {
//                qDebug() << QString("Enough blink reports tid 0x%1, seq 0x%2")
//                            .arg(tid,2,16,QChar('0'))
//                            .arg(seq,2,16,QChar('0'));

                //convert slave anchors' timestamp to master's time domain
                //***IF AND ONLY IF** the slave is still in synchornisation with the master
                int cnvtClkNum = 0;
                QVector<int> alist = blinkReport.recvAncList;
                for (int i = 0; i < alist.size(); i++)
                {
                    int aid = alist.at(i);

                    if ( aid == SYS_MASTER_ID )
                    {
                        cnvtClkNum++;
                    }
                    else
                    {
                        QString ancPairKeyStr = QString("%1:%2").arg(SYS_MASTER_ID).arg(aid);

                        if ( !blinkReport.cnvtAncList.contains(aid) && _ccpDb.contains(ancPairKeyStr) )
                        {
                            int syncseq = _ccpDb.value(ancPairKeyStr).seq;

                            if (syncseq == _ccpseq || syncseq == _ccpseq+1 )
                            {
                                qDebug() << QString("== Convert clock aid 0x%1, tid 0x%2, seq 0x%3.")
                                            .arg(aid,2,16,QChar('0'))
                                            .arg(tid,2,16,QChar('0'))
                                            .arg(seq,2,16,QChar('0'));


                                QString sBlinkTimestampKeyString = QString("%1:%2").arg(aid).arg(tid); //***** SHOULD THE KEY BE COMBINATION OF AID, TID AND SEQ *****//
                                QString mBlinkTimestampKeyString = QString("%1:%2").arg(SYS_MASTER_ID).arg(tid);
                                double clkSkew = _ccpkfDb.value(ancPairKeyStr).rel_freq_skew_hat;
                                timestamp_t s_recvBlinkTs = _recvBlinkTimestamp.value(sBlinkTimestampKeyString).timestamp;
                                timestamp_t s_lastSyncTs = _ccpDb.value(ancPairKeyStr).slave_clk;
                                if (s_recvBlinkTs < s_lastSyncTs)
                                {
                                    // It looks like the master has stopped synchronising clock.
                                    // So we move the received blink timestamp forward for one clock wrap-around period
                                    s_recvBlinkTs = s_recvBlinkTs + CLOCK_WRAP_PERIOD;
                                }
                                timestamp_t t2LocalDeltaTS = s_recvBlinkTs - s_lastSyncTs;
                                timestamp_t t1LocalDeltaTS = t2LocalDeltaTS /clkSkew;
                                timestamp_t sCnvtTS = _ccpDb.value(ancPairKeyStr).master_clk + t1LocalDeltaTS;
                                while (sCnvtTS > CLOCK_WRAP_PERIOD)
                                    sCnvtTS -= CLOCK_WRAP_PERIOD;

                                _cnvtRecvBlinkTimestamp.insert(sBlinkTimestampKeyString, sCnvtTS);

                                double t2LocalDeltaSec = convertdevicetimetosecu(t2LocalDeltaTS);
                                double t1LocalDeltaSec = t2LocalDeltaSec / clkSkew;
                                double sCnvtSec = convertdevicetimetosecu(_ccpDb.value(ancPairKeyStr).master_clk) + t1LocalDeltaSec;
                                if (sCnvtSec > CLOCK_WRAP_PERIOD_SEC)
                                    sCnvtSec -= CLOCK_WRAP_PERIOD_SEC;
                                _cnvtRecvBlinkTimeInSeconds.insert(sBlinkTimestampKeyString, sCnvtSec);

                                blinkReport.cnvtAncList.append(aid);
                                cnvtClkNum++;

                                if (isTagBlinkReceived(mBlinkTimestampKeyString, seq))
                                {
                                    timestamp_t mRecvBlinkTimestamp = _recvBlinkTimestamp.value(mBlinkTimestampKeyString).timestamp;
                                    double      mRecvBlinkTsSec = convertdevicetimetosecu(mRecvBlinkTimestamp);
                                    writeToDbgFile(QString("Convert sclk,aid,0x%1,tid,0x%2,seq,0x%3,recvBlinkTsHex,0x%4,sCnvtTsHex,0x%5,sCnvtSec,%6,mRecvBlinkTsHex,0x%7,mRecvBlinkTsSec,%8")
                                                   .arg(aid,2,16,QChar('0'))
                                                   .arg(tid,2,16,QChar('0'))
                                                   .arg(seq,2,16,QChar('0'))
                                                   .arg(s_recvBlinkTs,10,16,QChar('0'))
                                                   .arg(sCnvtTS,10,16,QChar('0'))
                                                   .arg(sCnvtSec,0,'E',12)
                                                   .arg(mRecvBlinkTimestamp,10,16,QChar('0'))
                                                   .arg(mRecvBlinkTsSec,0,'E',12));
                                }
                                else {
                                    writeToDbgFile(QString("Convert sclk,aid,0x%1,tid,0x%2,seq,0x%3,recvBlinkTsHex,0x%4,sCnvtTsHex,0x%5,sCnvtSec,%6")
                                                   .arg(aid,2,16,QChar('0'))
                                                   .arg(tid,2,16,QChar('0'))
                                                   .arg(seq,2,16,QChar('0'))
                                                   .arg(s_recvBlinkTs,10,16,QChar('0'))
                                                   .arg(sCnvtTS,10,16,QChar('0'))
                                                   .arg(sCnvtSec,0,'E',12));
                                }

                            } //if (syncseq == _ccpseq || syncseq == _ccpseq+1 )

                        } //if ( !blinkReport.cnvtAncList.contains(aid) && _ccpDb.contains(ancPairKeyStr) )

                    } //else (i.e., aid != SYS_MASTER_ID)

                } //for(i < alist.size())

                if (cnvtClkNum >= MIN_REQUIRED_ANC_NUM || blinkReport.cnvtAncList.length() >= MIN_REQUIRED_ANC_NUM)
                {
                    //proceed to compute tag position
                    writeToDbgFile(QString("RTLSClient::processTagBlink(): Proceed to compute tag position."));


                }
            } //if (blinkReport.count > MIN_REQUIRED_ANC_NUM)
        } //if (!blinkReport.ancList.contains(aid))

        _tagBlinkReport.insert(tid, blinkReport);
    }
}


bool RTLSClient::isTagBlinkReceived(QString str, int seq)
{
    return _recvBlinkTimestamp.contains(str) && _recvBlinkTimestamp.value(str).seq == seq;
}


void RTLSClient::insertRecvTagBlinkTimestamp (int aid, int tid, int seq, timestamp_t localTimestamp)
{
    QString str = QString("%1:%2").arg(aid).arg(tid);
    tag_blink_info_t blinkInfo;
    blinkInfo.seq = seq;
    blinkInfo.timestamp = localTimestamp;

    _recvBlinkTimestamp.insert(str,blinkInfo);
}


void RTLSClient::insertCCPDb (int mid, int aid, int seq, timestamp_t masterTimestamp, timestamp_t slaveTimestamp)
{
    QString str = QString("%1:%2").arg(mid).arg(aid);
    ccp_db_struct_t data;
    data.seq = seq;
    data.master_clk = masterTimestamp;
    data.slave_clk = slaveTimestamp;

    _ccpDb.insert(str, data);
}


void RTLSClient::insertCCPKFDbAndUpdate(int mid, int aid, int seq, timestamp_t masterPrevTimestamp, timestamp_t masterCurrTimestamp, timestamp_t receivedTimestamp)
{
    QString str = QString("%1:%2").arg(mid).arg(aid);
    ccp_kfdata_struct_t  ccpkfdata;

    if (!_ccpkfDb.contains(str))
    {
        //The first clock sync between this pair of mid and aid
        qDebug() << QString("Init CCP KF data of master 0x%1 and slave 0x%2.")
                 .arg(mid,2,16,QChar('0')).arg(aid,2,16,QChar('0'));

        ccpkfdata = initCCPKFData(mid, aid, seq, masterPrevTimestamp, masterCurrTimestamp, receivedTimestamp);
    }
    else {
        ccpkfdata = _ccpkfDb.value(str);
        ccpkfdata.ccp_period = convertdevicetimetosecu(clocksubtraction(masterCurrTimestamp, masterPrevTimestamp));
        ccpkfdata.slave_clk_reading = convertdevicetimetosecu(receivedTimestamp);

        qDebug() << QString("clock sync, mid 0x%1, sid 0x%2, seq 0x%3")
                    .arg(mid,2,16,QChar('0'))
                    .arg(aid,2,16,QChar('0'))
                    .arg(seq,2,16,QChar('0'));

        int r = getNewEstimate (&ccpkfdata);
        if (r == KF_SUCCESS)
        {
            writeToDbgFile(QString("getNewEstimate,mid,0x%1,aid,0x%2,seq,0x%3,s_ts_reading,0x%4,s_sec_reading,%5,s_sec_hat,%6,relf_hat,%7,m_ccp_period,%8")
                           .arg(mid,2,16,QChar('0'))
                           .arg(aid,2,16,QChar('0'))
                           .arg(seq,2,16,QChar('0'))
                           .arg(receivedTimestamp,10,16,QChar('0'))
                           .arg(ccpkfdata.slave_clk_reading,0,'f',12)
                           .arg(ccpkfdata.slave_clk_hat,0,'f',12)
                           .arg(ccpkfdata.rel_freq_skew_hat)
                           .arg(ccpkfdata.ccp_period,0,'f',12));
        }
        else {
            writeToDbgFile(QString("getNewEstimate fails with error code %1").arg(r));
        }
    }

    _ccpkfDb.insert(str, ccpkfdata);
}


ccp_kfdata_struct_t RTLSClient::initCCPKFData(int mid, int aid, int seq, timestamp_t masterPrevTimestamp, timestamp_t masterCurrTimestamp, timestamp_t slaveTimestamp)
{
    ccp_kfdata_struct_t data;
    data.P00 = 0;
    data.P01 = 0;
    data.P10 = 0;
    data.P11 = 0;
    data.slave_clk_hat = convertdevicetimetosecu(slaveTimestamp);
    data.rel_freq_skew_hat = 1;
    data.ccp_period = convertdevicetimetosecu(clocksubtraction(masterCurrTimestamp, masterPrevTimestamp));
    data.lastUpdateSuccess = true;
    writeToDbgFile(QString("initCCPKFData,mid,0x%1,aid,0x%2,seq,0x%3,t2_hat,%4,relf_hat,%5,period,%6")
                   .arg(mid,2,16,QChar('0'))
                   .arg(aid,2,16,QChar('0'))
                   .arg(seq,2,16,QChar('0'))
                   .arg(data.slave_clk_hat)
                   .arg(data.rel_freq_skew_hat)
                   .arg(data.ccp_period));

    return data;
}


void RTLSClient::openLogFile()
{
    QDateTime now = QDateTime::currentDateTime();
    QString prefix = "/Users/charuwalee/Development/qt_workspace/LocationEngine/logs/"+now.toString("yyyyMMdd_hhmmss");
    openLogFile(prefix);
//    _logFilePath = "/Users/charuwalee/Development/qt_workspace/LocationEngine/logs/"+now.toString("yyyyMMdd_hhmmss")+"_RTLS_log.txt";
//    _file = new QFile(_logFilePath);


//    if (!_file->open(QIODevice::WriteOnly | QIODevice::Text))
//    {
//        qDebug() << "Error Cannot read file " << _logFilePath;
//        qDebug() << "\t error string: " << _file->errorString();
//    }

//#if (DEBUG_FILE==1)
//    QString filenameDbg("/Users/charuwalee/Development/qt_workspace/LocationEngine/logs/"+now.toString("yyyyMMdd_hhmmss")+"_RTLS_log_dbg.txt");
//    _fileDbg = new QFile(filenameDbg);
//    if (!_fileDbg->open(QIODevice::WriteOnly | QIODevice::Text))
//    {
//        qDebug() << "Error: Cannot read file " << filenameDbg;
//        qDebug() << "\t error string: " << _fileDbg->errorString();
//    }
//#endif
}



void RTLSClient::openLogFile(QString prefix)
{
#if (!DBG_READ_FROM_INPUT_FILE)
    _logFilePath = prefix +"_RTLS_log.txt";
    _file = new QFile(_logFilePath);


    if (!_file->open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Error Cannot read file " << _logFilePath;
        qDebug() << "\t error string: " << _file->errorString();
    }

#endif

#if (GEN_DBG_FILE)
    QString filenameDbg(prefix + "_RTLS_log_dbg.txt");
    _fileDbg = new QFile(filenameDbg);
    if (!_fileDbg->open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug() << "Error: Cannot read file " << filenameDbg;
        qDebug() << "\t error string: " << _fileDbg->errorString();

    }
#endif
}



void RTLSClient::closeLogFile(void)
{
    if(_file)
    {
        _file->flush();
        _file->close();
        _file = nullptr ;
    }

#if (GEN_DBG_FILE)
    if(_fileDbg)
    {
        _fileDbg->flush();
        _fileDbg->close();
        _fileDbg = nullptr ;
    }
#endif
}


void RTLSClient::writeToLogFile(QString str)
{
    if (_file)
    {
        QTextStream ts(_file);
        ts << str << "\n";
    }
}


void RTLSClient::writeToLogFile(QByteArray data)
{
    if (_file)
    {
        QTextStream ts(_file);
        QString s(data);
        ts << s << "\n";
    }
}



void RTLSClient::writeToDbgFile(QString str)
{
    #if (GEN_DBG_FILE)
    if (_fileDbg)
    {
        QTextStream ts(_fileDbg);
        ts << str << "\n";
    }
    #endif
}

void RTLSClient::writeToDbgFile(QByteArray data)
{
    #if (GEN_DBG_FILE)
    if (_fileDbg)
    {
        QTextStream ts(_fileDbg);
        QString s(data);
        ts << s << "\n";
    }
    #endif
}
