#include "mainWindow.h"
#include "ui_mainwindow.h"
#include "rtlsdisplayapplication.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //connect(ui->logInputButton, SIGNAL(toggled), RTLSDisplayApplication::instance(), SLOT(readDataFromFile()));
}

MainWindow::~MainWindow()
{
    qDebug() << "Deleting main window.";
    delete ui;
}

void MainWindow::on_logInputButton_clicked(bool checked)
{
    if (checked) {
        ui->logInputButton->setText("Stop");
    } else {
        ui->logInputButton->setText("Start");
    }
}

void MainWindow::displayInputData() {
    QString line;
    ui->inputDataDisplayPane->setPlainText(line/*input data read from serial port*/);
}

