#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void toggled(bool);

private slots:
    void on_logInputButton_clicked(bool);

private:
    Ui::MainWindow *ui;
    void displayInputData();
};

#endif // LOCATIONENGINE_H
